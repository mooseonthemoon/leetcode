class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        total = 0 
        i = 0
        subStr = ""
        symbols = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
        speCase = {"IV": 4, "IX": 9, "XL": 40, "XC": 90, "CD": 400, "CM": 900}
        
        while i < len(s):
            subStr = s[i:i+2]
            if subStr in speCase:
                total = total + speCase[subStr]
                subStr = ""
                i = i + 2
            else:
                total = total + symbols[subStr[0]]
                subStr = ""
                i = i + 1
                
        return total
        