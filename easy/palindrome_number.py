class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        
        # First case, omitting all negative integer cases
        # Main logic, convert int to string, split into even
        # and odd case, compare val to corresponding negative val.
        # If even iterate half of length, if odd iterate the floor of length
        if x < 0:
            return False
        else:
            comparison = str(x)
            compare_length = len(comparison) / 2
            
            if isinstance(compare_length, int):
                for i in range(compare_length):
                    if comparison[i] == comparison[-1-i]:
                        continue
                    else:
                        return False
            else:
                floor = int(compare_length)
                for i in range(floor):
                    if comparison[i] == comparison[-1-i]:
                        continue
                    else:
                        return False
            return True

class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        
        # New solution while keeping the integer type for x, follow this equation
        
        if x < 0:
            return False
        elif x == 0:
            digits = 1
        else:
            digits = int(math.log10(x)) + 1
        
        # Break up equation into sections
        total = 0
        for i in range(digits):
            sec_one = 10**(digits-1-i)
            sec_two = int(x/(10**i))
            sec_three = 10 * int(0.1 * int(x/(10**i)))
            
            sec_two = sec_two - sec_three
            sec_one = sec_one * sec_two
            
            total = total + sec_one
            
        if total == x:
            return True
        else:
            return False

# This solution uses less memory but runs slower, a bit of a tradeoff but both of these segments
# could be optimized
