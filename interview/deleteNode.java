/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public void deleteNode(ListNode node) {
        ListNode previous = node;
        ListNode temporary = node.next;
        previous.val = temporary.val;
        previous.next = temporary.next;
    }
}