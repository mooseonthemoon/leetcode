// Question: https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/879/

class Solution {
    public void reverseString(char[] s) {
        // Split this up into two cases, one for even array and one for odd
        // Even case first, then the odd case
        int arr_end = s.length-1; // To keep track of array's end, no neg indexing
        char temp = ' ';
        if (s.length % 2 == 0) {
            for (int i = 0; i < s.length/2; i++) {
                temp = s[arr_end];
                s[arr_end] = s[i];
                s[i] = temp;
                arr_end -= 1;
            }
        } else {
            for (int i = 0; i < Math.floor(s.length/2); i++) {
                temp = s[arr_end];
                s[arr_end] = s[i];
                s[i] = temp;
                arr_end -= 1;
            }
        }
    }
}