class Solution {
    public int removeDuplicates(int[] nums) {
        int fin_len = 0; // Returns final length
        int arr_change = 1; // To update array after change in int
        
        // Edge cases
        if (nums.length == 0) {
            return fin_len;
        } else if (nums.length == 1) {
            return 1;
        }
        
        fin_len += 1;
        
        // Assuming array is at least 2 in length
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                continue;
            } else {
                fin_len += 1;
                nums[arr_change] = nums[i];
                arr_change += 1;
            }
        }
        return fin_len;
    }
}