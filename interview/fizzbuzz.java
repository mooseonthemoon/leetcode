class Solution {
    public List<String> fizzBuzz(int n) {
        List<String> final_list = new ArrayList<String>();
        String curr_val = "";
        
        for (int i = 1; i < n + 1; i++) {
            curr_val = (i % 3 == 0) ? "Fizz" : "";
            curr_val = (i % 5 == 0) ? curr_val + "Buzz" : curr_val;
            
            if (curr_val == "") {
                curr_val = Integer.toString(i);
            }
            
            final_list.add(curr_val);
        }
        
        return final_list;
    }
}